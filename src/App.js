import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import AdminDashboard from "./pages/AdminDashboard"
import AddProduct from "./pages/AddProduct"
import EditProduct from "./pages/EditProduct"
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import AllUsers from "./pages/AllUsers";
import Cart from "./pages/Cart";
import OrderHistory from "./pages/OrderHistory";

import {Container} from "react-bootstrap";
import './App.css';

function App() {

 
  const [user, setUser] = useState({
 
    id: null,
    isAdmin: null
  })


  console.log(user);

 
  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

 
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        "Content-Type":"application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
          setUser({
        
            id: data._id,
            isAdmin: data.isAdmin
          })
      }

      else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      
        <Router>
          <AppNavbar />
          <Container fluid>
          
              <Routes>
             
                  <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/admin" element={<AdminDashboard />} />
                  <Route exact path ="/allUsers" element={<AllUsers />} />
                  <Route exact path="/addProduct" element={<AddProduct />}/>
                  <Route exact path = "/cart" element={<Cart />} />
                  <Route exact path="/editProduct/:productId" element={<EditProduct />}/>
                  <Route exact path ="/products" element={<Products />} />
                  <Route exact path ="/products/:productId" element={<ProductView />} />
                  <Route exact path ="/register" element={<Register />} />
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="/orderHistory" element={<OrderHistory />} />
                  <Route exact path ="*" element={<Error />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}



export default App;
