import { useState, useEffect } from 'react';
import {Container, Table, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Link} from "react-router-dom";



export default function CheckUsers() {

    const [users, setUsers] = useState([]);
    const [reload, setReload] = useState(false);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUsers(
                data.map(user => {
                    return (
                        <tr key={user._id} className={user.isAdmin ? 'deliver-success' : 'bg-light'}>
                            <td>{user._id}</td>
                            <td>{user.lastName}</td>
                            <td>{user.firstName}</td>
                            <td>{user.email}</td>
                            <td>{user.isAdmin ? 'Admin' : 'User'}</td>
                            <td>
                                {
                                    (user.isAdmin)
                                        ?   <Button size="sm" className='  custom-deliver-disabled'disabled>Admin</Button>

                                        :   

                                        <>
                                        <Button size="sm" className='  custom-button-primary custom-deliver-button' onClick={()=>makeAdmin(user._id)}>Make Admin</Button>

                                        </>
                                    }
                                
                            </td>
                        </tr>
                    )
                })
            )
        })

    }, [reload])

    const makeAdmin = (userId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/makeAdmin`, {
            method: 'PATCH',
            headers: {
            	"Content-Type": "application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            	body: JSON.stringify({
			   isAdmin: true
			})
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                Swal.fire({
                    title: 'User is now an Admin',
                    icon: 'success',
                });
                setReload(!reload);
            }
            else {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error',
                });
            }
        })
    }

    return (
        <>
            <div className=''>
                <Container className="custom-account-wrapper">
                    <h1 className='text-center mb-5'>All Users:</h1>
                    <div >
                        <Table >
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                       
                                </tr>
                            </thead>
                            <tbody>
                                {users}
                            </tbody>
                        </Table>
                    </div>

                    <Button as={Link}to="/admin" size="sm" variant="success">Back to Admin Dashboard</Button>
                </Container>
            </div>
        </>
    );

};
