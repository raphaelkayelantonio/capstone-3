import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){


	const { user } = useContext(UserContext);

	
	const navigate = useNavigate();


	const { productId } = useParams();

	
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [orderCount, setOrderCount] = useState(1);
	const [price, setPrice] = useState(0);
	const [amount, setAmount] = useState(0);
	const [stocks, setStocks] = useState(0);


	const enroll = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				orderCount: orderCount,
				stocks:stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Succesfully ordered!",
					icon: "success",
					text: "You have successfully ordered this product."
				})
				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);

		})

	}, [productId])

	useEffect(() =>{
		console.log(orderCount);
		setAmount(price*orderCount);

	}, [orderCount])

	const handleDecrement = (card_id) => {
		setOrderCount(orderCount-1)
		setStocks(stocks+1)
		
	}

	const handleIncrement = (card_id) => {
		setOrderCount(orderCount+1)
		setStocks(stocks-1)

	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle className="mt-2">Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<div className="input-group">
								<button type="button" onClick={() => handleDecrement(productId)} className="input-group-text">-</button>
								<div className="form-control text-center">{orderCount}</div>
								<button type="button" onClick={() => handleIncrement(productId)} className="input-group-text">+</button>
							</div>
							<Card.Subtitle className="mt-2">Total Amount:</Card.Subtitle>
							<Card.Text>PhP {amount}</Card.Text>
							<Card.Subtitle className="mt-2">Stocks:</Card.Subtitle>
							<Card.Text>{stocks}</Card.Text>
							
							<div className="d-grid gap-2">
							{
								(user.id !== null)
								?
								<Button variant="primary" size="lg" onClick={() => enroll(productId)}>Order</Button>
								:
								<Button as={Link} to="/login" variant="primary" size="lg">Login to Order</Button>
							}
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
