import { useContext, useState, useEffect } from "react";
import {Button, Table} from "react-bootstrap";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";


export default function AddToCart(){
    const {user} = useContext(UserContext);
    const [userOrder, setUserOrder] = useState([]);
    let i = 1;

    const fetchData = () =>{
            fetch(`${process.env.REACT_APP_API_URL}/users/usersOrder`,{
                headers:{
                    "Content-Type":"application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            })
            .then(res => res.json())
            .then(data => {

                console.log(data)
                setUserOrder(data.orders.map((order,index) => {
                    
                    return (
                        <tr key={order._id}>
                              <td>{order._id}</td>
                                <td>Order #{index+1}: {order.enrolledOn}</td>
                                    <td>Product Name: {order.productId}</td>
                                    {/*<p>Quantity: {order.quantity}</p>
                                    <h6>Total Amount: {order.totalAmount}</h6>*/}
                        </tr>


                        )

                }))
            })
            }
    useEffect(()=>{
            
            fetchData();
        }, [])

    return (
        (user.id!==null)
        ?
        <>
            <div className="mt-5 mb-3 text-center">
                <h1>Orders</h1>
                
            </div>

            <div>
                 <Table >
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                               {/*     <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Email</th>
                                    <th>Role</th>*/}
                                       
                                </tr>
                            </thead>
                            <tbody>
                                {userOrder}
                            </tbody>
                        </Table>

            </div>
            

        </>
        :
        <Navigate to="/" />




        )
}