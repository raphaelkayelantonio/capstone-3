import { useState, useEffect } from 'react';
import {Container, Table, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Link} from "react-router-dom";



export default function History() {

    const [order, setOrder] = useState([]);
   

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
            headers: {
                "Content-Type":"application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setOrder(
                data.map(order => {
                    return (
                        <tr key={order._id}>
                            <td>{order._id}</td>
                            <td>{order.lastName}</td>
                            <td>{order.firstName}</td>
                            <td>{order.email}</td>
                            <td>{order.productId}</td>
                        </tr>
                    )
                })
            )
        })

    }, [order])

   
    return (
        <>
            <div className=''>
                <Container className="custom-account-wrapper">
                    <h1 className='text-center mb-5'>All Users:</h1>
                    <div >
                        <Table >
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Email</th>
                                    <th>Ordered Product</th>
                                       
                                </tr>
                            </thead>
                            <tbody>
                                {order}
                            </tbody>
                        </Table>
                    </div>

                    <Button as={Link}to="/admin" size="sm" variant="success">Back to Admin Dashboard</Button>
                </Container>
            </div>
        </>
    );

};
